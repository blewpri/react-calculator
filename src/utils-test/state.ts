import { Store } from 'state';
import { getInitialCalculator } from 'utils';
import createMockStore from 'redux-mock-store';

export const testState: Store = {
    calculator: getInitialCalculator()
};

export const createTestStore = () => {
    const mockStore = createMockStore<Store, any>([]);
    return mockStore(testState);
};