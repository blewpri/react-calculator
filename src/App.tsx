import React from 'react';
import { CalculatorContainer } from './components';

export const App: React.FC = () => {
  return (
    <CalculatorContainer /> 
  );
}
