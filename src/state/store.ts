import { combineReducers, createStore } from 'redux';
import { ICalculator } from 'utils';
import { calculatorReducer } from './calculator.reducer';
import { composeWithDevTools } from 'redux-devtools-extension';

export interface Store {
    calculator: ICalculator
}

export const store = createStore(
    combineReducers({ 
        calculator: calculatorReducer 
    }),
    // TODO: USE ENV switch of devtool in `prod`...
    composeWithDevTools()
);