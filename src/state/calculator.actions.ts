import * as types from './calculator.types';
import { CalculatorArg, operator } from 'utils';

export const getAction = (arg: CalculatorArg): types.CalculatorActionTypes => {
    if (typeof(arg) === 'number') {
        return appendNumber(arg)
    } else { 
        switch (arg) {
            case operator.clear:
                return clear();
            case operator.decimal:
                return appendDecimalPlace();
            case operator.add:
                return add();
            case operator.divide:
                return divide();
            case operator.equals:
                return equals();
            case operator.invert:
                return invert();
            case operator.multiply:
                return multiply();
            case operator.percent:
                return precentage();
            case operator.subtract:
                return subtract();
            default:
                return noAction();
        }
    }
}

export const equals = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_EQUALS
    };
};

export const add = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_ADD
    };
};

export const subtract = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_SUBTRACT
    };
};

export const multiply = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_MULTIPLY
    };
};

export const divide = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_DIVIDE
    };
};

export const precentage = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_PERCENTAGE
    };
};

export const invert = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_INVERT
    };
};

export const clear = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_CLEAR
    };
};

export const appendNumber = (value: number): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_APPEND_NUMBER,
        payload: value
    };
};

export const appendDecimalPlace = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_APPEND_DECIMAL_PLACE
    };
};

export const noAction = (): types.CalculatorActionTypes => {
    return {
        type: types.CALCULATOR_NO_ACTION
    };
};