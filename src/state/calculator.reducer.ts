import { 
    operator, ICalculator, storeValueIfOperatorActive, 
    concat, calculate, getInitialCalculator } from 'utils';
import * as types from './calculator.types';

const initialState = getInitialCalculator();

export const calculatorReducer = (
    state: ICalculator = { ...initialState }, 
    action: types.CalculatorActionTypes): ICalculator => {
    
    let newState: ICalculator;
    switch (action.type) {
        
        case types.CALCULATOR_APPEND_DECIMAL_PLACE:
            newState = { ...state };
            newState.current = (state.current + '').includes('.') ?
                newState.current : 
                `${newState.current}.`;
            return newState;
        case types.CALCULATOR_APPEND_NUMBER:
            newState = { ...state };
            newState = storeValueIfOperatorActive(newState);
            newState.current = concat(newState.current, action.payload);
            return newState;
        case types.CALCULATOR_CLEAR:
            return { ...initialState }
        case types.CALCULATOR_ADD:
            return calculate({
                ...state,
                operator: operator.add, 
            });
        case types.CALCULATOR_SUBTRACT:
            return calculate({
                ...state,
                operator: operator.subtract, 
            });
        case types.CALCULATOR_EQUALS:
            return calculate({
                ...state, 
            }, true);
        case types.CALCULATOR_DIVIDE:
        case types.CALCULATOR_MULTIPLY:
        case types.CALCULATOR_INVERT:
        case types.CALCULATOR_PERCENTAGE:
            console.info(`⛔ ${action.type} FEATURE not implemented!`);
            return state;
        default:
            return state;  
    }

};