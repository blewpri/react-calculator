export const CALCULATOR_APPEND_NUMBER = 'CALCULATOR_APPEND_NUMBER';
export const CALCULATOR_APPEND_DECIMAL_PLACE = 'CALCULATOR_APPEND_DECIMAL_PLACE';
export const CALCULATOR_ADD = 'CALCULATOR_ADD';
export const CALCULATOR_SUBTRACT = 'CALCULATOR_SUBTRACT';
export const CALCULATOR_MULTIPLY = 'CALCULATOR_MULTIPLY';
export const CALCULATOR_DIVIDE = 'CALCULATOR_DIVIDE';
export const CALCULATOR_PERCENTAGE = 'CALCULATOR_PERCENTAGE';
export const CALCULATOR_INVERT = 'CALCULATOR_INVERT';
export const CALCULATOR_CLEAR = 'CALCULATOR_CLEAR';
export const CALCULATOR_EQUALS = 'CALCULATOR_EQUALS';
export const CALCULATOR_NO_ACTION = 'CALCULATOR_NO_ACTION'; 

interface CalculatorAppendNumberAction {
    type: typeof CALCULATOR_APPEND_NUMBER
    payload: number  
}

interface CalculatorAppendDecimalPlace {
    type: typeof CALCULATOR_APPEND_DECIMAL_PLACE
}

interface CalculatorAddAction {
    type: typeof CALCULATOR_ADD
}

interface CalculatorAddAction {
    type: typeof CALCULATOR_ADD
}

interface CalculatorSubtractAction {
    type: typeof CALCULATOR_SUBTRACT
}

interface CalculatorMultiplyAction {
    type: typeof CALCULATOR_MULTIPLY
}

interface CalculatorDivideAction {
    type: typeof CALCULATOR_DIVIDE
}

interface CalculatorPercentageAction {
    type: typeof CALCULATOR_PERCENTAGE
}

interface CalculatorInvertAction {
    type: typeof CALCULATOR_INVERT
}

interface CalculatorClearAction {
    type: typeof CALCULATOR_CLEAR
}

interface CalculatorEqualsAction {
    type: typeof CALCULATOR_EQUALS
}

interface CalculatorNoAction {
    type: typeof CALCULATOR_NO_ACTION
}

export type CalculatorActionTypes = 
    CalculatorAppendNumberAction | CalculatorAppendDecimalPlace |
    CalculatorAddAction | CalculatorSubtractAction |
    CalculatorMultiplyAction | CalculatorDivideAction |
    CalculatorPercentageAction | CalculatorInvertAction |
    CalculatorClearAction | CalculatorEqualsAction | 
    CalculatorNoAction;
    