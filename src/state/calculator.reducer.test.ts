import deepFreeze from 'deep-freeze';
import { ICalculator, getInitialCalculator, operator } from 'utils';
import { calculatorReducer } from './calculator.reducer';
import * as actions from './calculator.actions';

describe('REDUX: Calculator Reducer', ()=> {

    let state: ICalculator;

    beforeEach(() => {
        state = getInitialCalculator(); 
    });

    describe('CALCULATOR_APPEND_DECIMAL_PLACE', () => {
        it('correctly adds a decimal place', ()=> {
            state.current = 10;
            deepFreeze(state);
            let newState = calculatorReducer(state, actions.appendDecimalPlace());
            expect(newState.current).toBe('10.');
        });

        it('will only add a single decimal place', ()=> {
            state.current = '10.2';
            deepFreeze(state);
            let newState = calculatorReducer(state, actions.appendDecimalPlace());
            expect(newState.current).toBe('10.2');
        });
    });

    describe('CALCULATOR_APPEND_NUMBER', () => {
        it('correctly appends one number to another', ()=> {
            state.current = 1;
            deepFreeze(state);
            let newState = calculatorReducer(state, actions.appendNumber(5));
            expect(newState.current).toBe(15);
        });
    });

    describe('CALCULATOR_CLEAR', () => {

        it('Clears all calc data', ()=> {
            state.current = 10;
            state.operatorValue = 5;
            state.operator = operator.add;
            deepFreeze(state);
            let newState = calculatorReducer(state, actions.clear());

            expect(newState.current).toBe(0);
            expect(newState.operatorValue).toBeNull();
            expect(newState.operator).toBeNull();
        });

    });

    describe('CALCULATOR_ADD', () => {

        it('Correctly adds the calc values together', ()=> {
            state.current = 10;
            state.operatorValue = 5;
            state.operator = operator.add;
            deepFreeze(state);
            let newState = calculatorReducer(state, actions.add());
            expect(newState.current).toBe(15);
        });

    });

    describe('CALCULATOR_SUBTRACT', () => {
        
        it('Correctly subtracts the calc values together', ()=> {
            state.current = 5;
            state.operatorValue = 10;
            state.operator = operator.subtract;
            deepFreeze(state);
            let newState = calculatorReducer(state, actions.subtract());
            expect(newState.current).toBe(5);
        });

    });

    describe('CALCULATOR_EQUALS', () => {

        it('Correctly does the math if the add operator is active', ()=> {
            state.current = 10;
            state.operatorValue = 5;
            state.operator = operator.add;
            deepFreeze(state);
            let newState = calculatorReducer(state, actions.equals());
            expect(newState.current).toBe(15);
        });

        it('Correctly does the math if the subtract operator is active', ()=> {
            state.current = 5;
            state.operatorValue = 10;
            state.operator = operator.subtract;
            deepFreeze(state);  
            let newState = calculatorReducer(state, actions.equals());
            expect(newState.current).toBe(5);
        });
    });

   
});

