export const operator = {
    multiply: String.fromCharCode(215),
    divide: String.fromCharCode(247),
    equals: String.fromCharCode(61),
    add: String.fromCharCode(43),
    subtract: String.fromCharCode(8722),
    percent: String.fromCharCode(37),
    invert: String.fromCharCode(177),
    clear: 'AC',
    decimal: '.'
};