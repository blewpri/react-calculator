import { operator } from './operator.model';

describe('Models > operator.model', () => {

    it('the add symbol should be correct', () => {
        expect(operator.add).toEqual('+');
    });

    it('the clear symbol should be correct', () => {
        expect(operator.clear).toEqual('AC');
    });

    it('the decimal symbol should be correct', () => {
        expect(operator.decimal).toEqual('.');
    });

    it('the divide symbol should be correct', () => {
        expect(operator.divide).toEqual('÷');
    });

    it('the equals symbol should be correct', () => {
        expect(operator.equals).toEqual('=');
    });

    it('the invert symbol should be correct', () => {
        expect(operator.invert).toEqual('±');
    });

    it('the multiply symbol should be correct', () => {
        expect(operator.multiply).toEqual('×');
    });

    it('the percent symbol should be correct', () => {
        expect(operator.percent).toEqual('%');
    });

    it('the subtract symbol should be correct', () => {
        expect(operator.subtract).toEqual('−');
    });

});
