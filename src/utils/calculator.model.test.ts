import * as calculator from './calculator.model';
import { operator } from './operator.model';

describe('Models > calculator.model', () => {
    
    let init: calculator.ICalculator;

    beforeEach(() => {
        init = calculator.getInitialCalculator();
    });

    describe('getInitialData()', () => {

        it('inital data is as expected', () => {
            init = calculator.getInitialCalculator();
            expect(init.current).toEqual(0);
            expect(init.operator).toEqual(null);
            expect(init.operatorValue).toEqual(null);
        });

    });

    describe('concat()', () => {

        it('joins two numbers together', () => {
            expect(calculator.concat(1,2)).toEqual(12);
            expect(calculator.concat(3,4)).toEqual(34);
            expect(calculator.concat(5,6)).toEqual(56);
        });

    });

    describe('storeValueInfOperatorActive()', () => {

        it('does not store the current calculator value if there is no active operator', () => {
            init.current = 10;
            init.operator = null;
            init = calculator.storeValueIfOperatorActive(init);
            expect(init.operatorValue).toBeNull();
        });

        it('does not store the current calculator value is there is already an operator value stored', () => {
            const operatorValue = 23;
            init.current = 12;
            init.operator = operator.add;
            init.operatorValue = operatorValue;
            init = calculator.storeValueIfOperatorActive(init);
            expect(init.operatorValue).toEqual(operatorValue);
        });

        it('stores the current value in the operator value slot if the is an active operator and no stored operator value', () => {
            const currentValue = 10;
            init.current = currentValue;
            init.operator = operator.add;
            init.operatorValue = null;
            init = calculator.storeValueIfOperatorActive(init);
            expect(init.operatorValue).toEqual(currentValue);
        });

    });

    describe('calculate()', () => { 

        it('does nothing if there is not active operator', ()=> {
            init.operator = null;
            init.operatorValue = null;
            let newValues = {
                ...calculator.calculate(init, false)
            };
            expect(init).toEqual(newValues); 
        });

        it('does add if add and a value are passed', ()=> {
            init.current = 5;
            init.operator = operator.add;
            init.operatorValue = 10;
            let newValues = {
                ...calculator.calculate(init, false)
            };
            expect(newValues.current).toEqual(15); 
        });

        it('does subtract if subtract and a value are passed', ()=> {
            init.current = 2;
            init.operator = operator.subtract;
            init.operatorValue = 5;
            let newValues = {
                ...calculator.calculate(init, false)
            };
            expect(newValues.current).toEqual(3); 
        });

        it('the last operator value is cleared after the calculation', ()=> {
            init.current = 2;
            init.operator = operator.subtract;
            init.operatorValue = 5;
            let newValues = {
                ...calculator.calculate(init, false)
            };
            expect(newValues.operatorValue).toBeNull(); 
        });

        it('the last operator is cleared if the clearOperator param is passed', ()=> {
            init.current = 2;
            init.operator = operator.subtract;
            init.operatorValue = 5;
            let newValues = {
                ...calculator.calculate(init, true)
            };
            expect(newValues.operator).toBeNull(); 
        });

    });
    
    describe('add()', () => { 

        it('just returns the current value if there is no operator value', () => {
            init.current = 2;
            init.operator = operator.add;
            init.operatorValue = null;
            expect(calculator.add(init)).toEqual(2); 
        });

        it('just works just fine if you pass a string current value', () => {
            init.current = '2';
            init.operator = operator.add;
            init.operatorValue = 3;
            expect(calculator.add(init)).toEqual(5); 
        });

    });

    describe('subtract()', () => { 

        it('just returns the current value if there is no operator value', () => {
            init.current = 2;
            init.operator = operator.subtract;
            init.operatorValue = null;
            expect(calculator.subtract(init)).toEqual(2);
        });

        it('just works just fine if you pass a string current value', () => {
            init.current = '1';
            init.operator = operator.subtract;
            init.operatorValue = 8;
            expect(calculator.subtract(init)).toEqual(7); 
        });

    });

    describe('parseFloat()', () => { 

        it('works with whole numbers', () => {
            expect(calculator.parseNumber('1')).toEqual(1); 
            expect(calculator.parseNumber('12')).toEqual(12);
            expect(calculator.parseNumber('123')).toEqual(123); 
        });

        it('works with floats', () => {
            expect(calculator.parseNumber('10.')).toEqual(10); 
            expect(calculator.parseNumber('10.1')).toEqual(10.1);
            expect(calculator.parseNumber('10.112')).toEqual(10.112);  
        });

    });
 

});