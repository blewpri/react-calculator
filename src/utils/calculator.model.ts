import { operator } from "./operator.model";

export type CalculatorArg = string | number;

export const getInitialCalculator = ():ICalculator => {
    return { current: 0, operator: null, operatorValue: null };
};

export interface ICalculator {
    current: number|string;
    operator: string|null;
    operatorValue: number|null;
};

export const concat = (value: string|number, valueOther: string|number) => {
    return parseNumber(`${value}${valueOther}`);
}

export const storeValueIfOperatorActive = (calc: ICalculator) => {
    if (calc.operator) {
        if (calc.operatorValue === null) {
            calc.operatorValue = parseNumber(calc.current);
            calc.current = 0;
        } 
    }
    return calc;
};

export const calculate = (calc: ICalculator, 
    clearOperator: boolean = false) => {

    if (calc.operator && calc.operatorValue) {
        switch(calc.operator) {
            case operator.add:
                return doCalculation(calc, add, clearOperator);
            case operator.subtract:
                return doCalculation(calc, subtract, clearOperator);
            default:
                return calc;
        }
    }
    return calc;
};

export const doCalculation = (calc: ICalculator, func: (calc: ICalculator) => void,
    clearOperator: boolean = false): ICalculator => {
    calc.current = parseNumber(func(calc));
    calc.operatorValue = null;
    if (clearOperator) {
        calc.operator = null;
        
    }
    return calc;
};

export const add = (calc: ICalculator) => 
    (calc.operatorValue) ?
        calc.operatorValue + parseNumber(calc.current): 
        parseNumber(calc.current);

export const subtract = (calc: ICalculator) => 
    (calc.operatorValue) ?
        calc.operatorValue - parseNumber(calc.current):
        parseNumber(calc.current);

export const parseNumber = (value: any) => 
    parseFloat(`${value}`);