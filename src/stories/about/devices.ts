export const devices = [
    { name: 'Desktop', args: { viewport: { defaultViewport: 'desktop' } } },
    { name: 'iPhone 5', args: { viewport: { defaultViewport: 'iphone5' } } },
    { name: 'iPhone XR', args: { viewport: { defaultViewport: 'iphonexr' } } }
];