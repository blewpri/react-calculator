import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { Calculator } from 'components';
import { getInitialCalculator } from 'utils';
import { devices } from 'stories/about/devices';

const calculator = getInitialCalculator();
const stories = storiesOf('Calculator', module);

devices.forEach(device => 
    stories.add(device.name, () => 
        <Calculator calculator={calculator} onButtonClick={action('button-click')} />, device.args));
