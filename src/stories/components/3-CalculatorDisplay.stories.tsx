import React from 'react';
import { storiesOf } from '@storybook/react';
import { CalculatorDisplay } from 'components';
import { getInitialCalculator } from 'utils';
import { devices } from 'stories/about/devices';

const calculator = getInitialCalculator();
const stories = storiesOf('Calculator Display', module);

devices.forEach(device => 
    stories.add(device.name, () => 
        <CalculatorDisplay calculator={calculator} />, device.args));