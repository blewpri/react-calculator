import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { getInitialCalculator } from 'utils';
import { CalculatorButtons } from 'components';
import { devices } from 'stories/about/devices';

const calculator = getInitialCalculator();
const stories = storiesOf('Calculator Buttons', module);

devices.forEach(device => 
    stories.add(device.name, () => 
        <CalculatorButtons calculator={calculator} onClick={action('button-click')} />, device.args));
