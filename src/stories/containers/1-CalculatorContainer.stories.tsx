import React from 'react';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import { getInitialCalculator } from 'utils';
import { CalculatorContainer } from 'components';
import { devices } from 'stories/about/devices';

const calculator = getInitialCalculator();
const stories = storiesOf('Calculator Container', module);

devices.forEach(device => 
    stories.add(device.name, () =>  
        <CalculatorContainer calculator={calculator} dispatch={action('button-click')}  />, device.args));
