import React from 'react'; 
import classNames from 'classnames';
import { operator, CalculatorArg, ICalculator } from 'utils';
import styles from './CalculatorButtons.module.scss';

export interface CalculatorButtonsProps {
    calculator: ICalculator
    onClick: (value: CalculatorArg) => void;
};

export const CalculatorButtons: React.FC<CalculatorButtonsProps> = (props) => {
    
    const buttons = [
        operator.clear, operator.invert, operator.percent, operator.divide, 
        1, 2, 3, operator.multiply,
        4, 5, 6, operator.subtract,
        7, 8, 9, operator.add,
        '', 0, operator.decimal, operator.equals
    ].map((value: number|string, index: number) => {

        const buttonClasses = classNames(
            styles.button, 
            { [styles.selected] : 
                (props.calculator.operator === value) && (value !== '') }
        );
        
        return (
            <button className={buttonClasses}
                type="button" key={index}
                data-test={`button-${value}`}
                onClick={(e) => props.onClick(value)}>
                {value}
            </button>
        )
    
    });
    
    return (
        <section className={styles.component}>
            { buttons }
        </section>
    );
};

export default CalculatorButtons;