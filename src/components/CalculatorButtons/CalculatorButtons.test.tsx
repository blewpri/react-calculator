import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, ShallowWrapper } from 'enzyme';
import { ICalculator, getInitialCalculator, operator } from 'utils';
import { CalculatorButtons } from './CalculatorButtons';

describe('<CalculatorButtons />', () => {

  let onClick: any;
  let div: any;
  let calculator: ICalculator;

  beforeEach(() => {
    onClick = jest.fn();
    calculator = getInitialCalculator();
    div = document.createElement('div');
  });

  it('[SMOKE - DEEP] renders without crashing', () => {
    ReactDOM.render(<CalculatorButtons 
      calculator={calculator}
      onClick={onClick} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('[SMOKE - SHALLOW] renders without crashing', () => {
    shallow(<CalculatorButtons 
      calculator={calculator}
      onClick={onClick} />);
  });

  describe('the buttons in this component', () => {
    
    let wrapper: ShallowWrapper;
    const buttonPrefix = 'button-';
    const buttons = [
      operator.clear, operator.invert, operator.percent, operator.divide, 
      1, 2, 3, operator.multiply,
      4, 5, 6, operator.subtract,
      7, 8, 9, operator.add,
      '', 0, operator.decimal, operator.equals
    ];

    beforeEach(() => {
      wrapper = shallow(<CalculatorButtons 
        calculator={calculator}
        onClick={onClick} />);
    });

    it('are all the ones we expect to see', () => {
      buttons.forEach(value => {
        const button = wrapper.find(`[data-test="${buttonPrefix}${value}"]`);
        expect(button.length).toBe(1);
      });
    });

    it('all have the correct text value', () => {
      buttons.forEach(value => {
        const button = wrapper.find(`[data-test="${buttonPrefix}${value}"]`);
        expect(button.text()).toBe(value+'');
      });
    });

    it('show pass the correct value in their args', () => {
      buttons.forEach(value => {
        const button = wrapper.find(`[data-test="${buttonPrefix}${value}"]`);
        button.simulate('click');
        expect(onClick).toHaveBeenCalledWith(value);
      });
    });

  });

});

