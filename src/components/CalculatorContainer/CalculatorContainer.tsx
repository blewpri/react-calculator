import React from 'react'; 

import { actions, Store } from 'state';
import { useSelector, useDispatch } from 'react-redux';
import { CalculatorArg } from 'utils';
import { Calculator as Calc } from '../Calculator/Calculator';
import styles from './CalculatorContainer.module.scss';

export interface CalculatorContainerProps {};

export const CalculatorContainer: React.FC<any> = (props) => {
    const calculator = useSelector((state: Store) => state.calculator);
    const dispatch = useDispatch();
    const onButtonClicked = (arg: CalculatorArg )=> 
        dispatch(actions.getAction(arg));
    
    return (
        <section className={styles.component}>
            <Calc 
                calculator={calculator}
                onButtonClick={(e) => onButtonClicked(e)} />
        </section>
    );
};