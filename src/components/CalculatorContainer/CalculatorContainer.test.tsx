import React from 'react';
import ReactDOM from 'react-dom';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { createTestStore } from 'utils-test';
import { CalculatorContainer } from './CalculatorContainer';

describe('<CalculatorContainer />', () => {

  let div: any;
  let testStore: any;

  beforeEach(() => {
    testStore = createTestStore();
    div = document.createElement('div');
  });

  it('[SMOKE] renders without crashing', () => {
    ReactDOM.render(
      <Provider store={testStore}>
        <CalculatorContainer />
      </Provider>, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('[SMOKE - SHALLOW] renders without crashing', () => {
    mount(
      <Provider store={testStore}>
        <CalculatorContainer />
      </Provider>);
  });

});
