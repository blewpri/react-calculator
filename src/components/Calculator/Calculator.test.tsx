import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { ICalculator, getInitialCalculator } from 'utils';
import { Calculator } from './Calculator';

describe('<Calculator />', () => {

  let onClick: any;
  let div: any;
  let calculator: ICalculator;

  beforeEach(() => {
    onClick = jest.fn();
    calculator = getInitialCalculator();
    div = document.createElement('div');
  });

  it('[SMOKE - DEEP] renders without crashing', () => {
    ReactDOM.render(<Calculator 
      calculator={calculator}
      onButtonClick={onClick} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('[SMOKE - SHALLOW] renders without crashing', () => {
    shallow(<Calculator 
      calculator={calculator}
      onButtonClick={onClick} />);
  });

});
