import React from 'react'; 
import { ICalculator, CalculatorArg } from 'utils';
import { CalculatorButtons } from '../CalculatorButtons/CalculatorButtons';
import { CalculatorDisplay } from '../CalculatorDisplay/CalculatorDisplay';
import styles from './Calculator.module.scss';

export interface CalculatorProps {
    calculator: ICalculator;  
    onButtonClick: (value: CalculatorArg) => void;
};

export const Calculator: React.FC<CalculatorProps> = (props) => {
    return (
        <div className={styles.component}>
            <div className="{styles.display}">
                <CalculatorDisplay calculator={props.calculator} />
            </div>
            <div className={styles.buttons}>
                <CalculatorButtons 
                    calculator={props.calculator}
                    onClick={ props.onButtonClick} />
            </div>
        </div>
    );
};

export default Calculator;