import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import { ICalculator, getInitialCalculator } from 'utils';
import { CalculatorDisplay } from './CalculatorDisplay';

describe('<CalculatorDisplay />', () => {

  let div: any;
  let calculator: ICalculator;

  beforeEach(() => {
    div = document.createElement('div');
    calculator = getInitialCalculator();
  });

  it('[SMOKE] renders without crashing', () => {
    ReactDOM.render(<CalculatorDisplay
      calculator={calculator} />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('[SMOKE - SHALLOW] renders without crashing', () => {
    shallow(<CalculatorDisplay
      calculator={calculator} />);
  });

  it('displays the current output of the calculator', () => {
    calculator.current = '12';
    const wrapper = shallow(
      <CalculatorDisplay
        calculator={calculator} />);
    const display = wrapper.find(`[data-test="calculatorDisplay"]`);     
    expect(display.length).toEqual(1);
    expect(display.text()).toBe(calculator.current);
  });

});
