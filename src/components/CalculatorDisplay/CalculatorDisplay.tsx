import React from 'react'; 
import { ICalculator } from 'utils';
import styles from './CalculatorDisplay.module.scss';

export interface CalculatorDisplayProps {
    calculator: ICalculator
};

export const CalculatorDisplay: React.FC<CalculatorDisplayProps> = (props) => {
    
    return (
        <section className={styles.component}
            title="Hello Display">

            <div data-test="calculatorDisplay" 
                className={styles.calculation}>
                {props.calculator.current}
            </div>
            
        </section>
    );
};

export default CalculatorDisplay;