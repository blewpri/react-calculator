# **React** Calculator App

A simple calculator App. 


## Quickstart

Run `npm i`, then:
* `npm run start` : Run app locally ([http://localhost:3000/](http://localhost:3000/))
* `npm run test:stats` : Run tests (with code coverage stats)
* `npm run storybook` : Spin up storybook to view components individually

## Key Technology

This project was built using React, Redux, Typescript, SASS (Used a [BEM](http://getbem.com/introduction/) style naming convention) Storybook, Jest, Enzyme.

## Functionality

As this exericse was timeboxed, the not all calculator functionality is complete. what follows is a brief summary...

### Completed

* **Numeric** Buttons [0-9]
* **Decimal** Button 
* **Add** Button (inc multiple addition)
* **Subtract** Button (inc multiple subtraction)
* **Equals** Button
* **Clear/AC** Button
* **Equal Experts Branding** Colours, Site Title, FavIcon & Logo

### Uncompleted - Core Functionality

* **Division** Button
* **Multiply** Button
* **Percentage** Button
* **Positive/Negative** Button
* Differentiate between **C** and **AC** Clear functionality

### Uncompleted - Code & Tests

* **Min/Max Number** Checks & Tests
* Tests for **more complex** calcuations

### Uncompleted - Additional Functionality

* **Keyboard** Input Support
* Adjust Calculator Display **Font Size** to allow for longer numbers on smaller screens.
* Accessibility Checks & Support for screen readers, etc..

## Equal Experts

Problem Version: **38ada8dd45ad9b9c3f5bae33cb41119c526dd243**

## About Project

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
